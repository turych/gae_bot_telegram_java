package turych.gae.bot.telegram.java;

import com.vk.api.sdk.client.TransportClient;
import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.ServiceActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.wall.WallPostFull;
import com.vk.api.sdk.objects.wall.responses.GetResponse;

import java.util.List;

public class VkWallApi {

    private Integer APP_ID = 6347589;
    private String CLIENT_SECRET = "CjU7lDAfOASJdc6c48Jb";
    private String TOKEN = "8b4379d88b4379d88b4379d81a8b23a29d88b438b4379d8d12862fec692180574005ea4";
    private Integer ownerId;

    private VkApiClient vk;
    private ServiceActor actor;

    public VkWallApi() {
        auth();
    }

    private void auth() {
        TransportClient transportClient = HttpTransportClient.getInstance();
        this.vk = new VkApiClient(transportClient);
        this.actor = new ServiceActor(APP_ID, CLIENT_SECRET, TOKEN);
    }

    public Integer getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    public List<WallPostFull> getPosts(Integer count, Integer offset){
        GetResponse wallPosts = null;
        try {
            wallPosts = vk.wall().get(actor).ownerId(getOwnerId()).count(count).offset(offset).execute();
            return wallPosts.getItems();
        } catch (ApiException | ClientException e) {
            e.printStackTrace();
        }
        return null;
    }
}
