package turych.gae.bot.telegram.java;

import com.vk.api.sdk.objects.wall.WallPostFull;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.util.*;

public class Sparta6mmBot extends TelegramLongPollingBot {

    private Integer VK_OWNER_ID = -56518400;

    @Override
    public String getBotUsername() {
        return "sparta6mmBot";
    }

    @Override
    public String getBotToken() {
        return "580317048:AAEVIisUE2BfCvTta-pHpjQ9wUFqNf0VTO0";
    }

    @Override
    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        if (message != null && message.hasText()) {

            String[] command = message.getText().split(" ");

            switch (command[0]) {
                case "/start":
                    sendMsg(message,"Привет. Я бот группы Спарта.\nДоступные копанды /help");
                    break;
                case "/help":
                    sendMsg(message,"/last - Получить последнюю публикацию группы.\n/last 1 - Получить предыдущую пере последней и т.д.");
                    break;

                case "/last":

                    int offset = command.length == 2 ? Integer.valueOf(command[1]) : 0;

                    WallPostFull post = getLastPost(1, offset);
                    sendMsg(message, post.getText() +
                        "\n[Открыть пост в vk](https://vk.com/sparta6mm?w=wall-56518400_" +
                        post.getId() + ")");
                    break;

                default:
                    sendMsg(message,"Я не знаю что ответить на это");
            }
        }
    }

    @SuppressWarnings("deprecation")
    private void sendMsg(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId());
        sendMessage.enableMarkdown(true);
        sendMessage.setText(text);
        try {
            sendMessage(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private WallPostFull getLastPost(int count, int offset){
        VkWallApi vkWall = new VkWallApi();
        vkWall.setOwnerId(VK_OWNER_ID);
        List<WallPostFull> posts = vkWall.getPosts(count, offset);
        return posts.get(0);
    }
}
